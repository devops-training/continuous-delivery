#!/bin/bash

set -e

export CONFIG_FOLDER=/go-server/config

mkdir -p $CONFIG_FOLDER

# INFO Store access tokens during deploy time
# We get access token from `oc secret` during deploy time and store it
# @see https://git-scm.com/book/gr/v2/Git-Tools-Credential-Storage
if [[ -n $GOCD_GITHUB_USERNAME || -n $GOCD_GITHUB_ACCESS_TOKEN ]]; then
  git config --global credential.helper "store --file ${CONFIG_FOLDER}/.git-credentials"
  cat <<EOF > ${CONFIG_FOLDER}/.git-credentials
https://${GOCD_GITHUB_USERNAME}:${GOCD_GITHUB_ACCESS_TOKEN}@github.com
EOF
fi
