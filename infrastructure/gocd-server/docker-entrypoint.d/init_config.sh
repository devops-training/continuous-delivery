#!/bin/bash
# This script overrides the initial GoCD configuration file
#
# This normally only happens one time after the server is started.
# From then on the config file is fully managed by GoCD.
#
# BACKGROUND
#
# The GoCD image does not respect the 'AGENT_AUTO_REGISTER_KEY' key
# anymore so we handle the whole XML file like this.
# We do not mount the file directly because GoCD will modify the contents which
# leads to modification and we want to protect the user from these git changes.
set -eu

GOCD_CONFIG_FILE=/godata/config/cruise-config.xml

if ! grep "<config-repo>" $GOCD_CONFIG_FILE; then
  echo "INFO Overriding $GOCD_CONFIG_FILE"
  cp /tmp/config/cruise-config.xml $GOCD_CONFIG_FILE
fi
