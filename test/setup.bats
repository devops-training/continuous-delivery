#!/usr/bin/env bats

load test_helper

@test "Override HOME for tests" {
  run echo $HOME
  [ "$status" -eq 0 ]
  echo "$output" | grep tmp/home$
}

@test "Respects CI environment" {
  skip
  export OSTYPE=ubuntu
  export CI=true
  run bash ./scripts/setup/verify.sh
  [ "$status" -eq 0 ]
  echo "$output" | grep "INFO Not installing anything"
}
