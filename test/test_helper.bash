#!/bin/bash

FIXTURES_HOME="${PWD}/tmp/home"
FIXTURES_OSTYPE="darwin"

setup() {
  rm -fr $FIXTURES_HOME
  mkdir -p ${FIXTURES_HOME}
  OLD_HOME=$HOME
  OLD_OSTYPE=$OSTYPE
  export HOME=$FIXTURES_HOME
  export OSTYPE=$FIXTURES_OSTYPE
}

teardown() {
  rm -fr $FIXTURES_HOME
  export HOME=$OLD_HOME
  export OSTYPE=$OLD_OSTYPE
}
